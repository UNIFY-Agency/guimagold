<!DOCTYPE html>
<html>
<head>

  <!-- Basic -->
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">

  <title>Maintenance Mode - Eurotecnologia</title>

  <meta name="keywords" content="Eurotecnologia" />
  <meta name="description" content="Eurotecnologia">
  <meta name="author" content="unify.pt">

  <!-- Favicon -->
  <link rel="shortcut icon" href="<?php echo URL_BASE; ?>/assets/img/favicon.ico" type="image/x-icon" />
  <link rel="apple-touch-icon" href="<?php echo URL_BASE; ?>/assets/img/apple-touch-icon.png">

  <!-- Mobile Metas -->
  <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1.0, shrink-to-fit=no">

  <!-- Web Fonts  -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800%7CShadows+Into+Light%7CPlayfair+Display:400" rel="stylesheet" type="text/css">
  <link href="<?php echo URL_BASE; ?>/assets/css/fonts.css" rel="stylesheet" type="text/css">
  <!-- Vendor CSS -->
  <link rel="stylesheet" href="<?php echo URL_BASE; ?>/assets/vendor/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="<?php echo URL_BASE; ?>/assets/vendor/fontawesome-free/css/all.min.css">
  <link rel="stylesheet" href="<?php echo URL_BASE; ?>/assets/vendor/animate/animate.min.css">
  <link rel="stylesheet" href="<?php echo URL_BASE; ?>/assets/vendor/simple-line-icons/css/simple-line-icons.min.css">
  <link rel="stylesheet" href="<?php echo URL_BASE; ?>/assets/vendor/owl.carousel/assets/owl.carousel.min.css">
  <link rel="stylesheet" href="<?php echo URL_BASE; ?>/assets/vendor/owl.carousel/assets/owl.theme.default.min.css">
  <link rel="stylesheet" href="<?php echo URL_BASE; ?>/assets/vendor/magnific-popup/magnific-popup.min.css">

  <!-- Theme CSS -->
  <link rel="stylesheet" href="<?php echo URL_BASE; ?>/assets/css/theme.css">
  <link rel="stylesheet" href="<?php echo URL_BASE; ?>/assets/css/theme-elements.css">
  <link rel="stylesheet" href="<?php echo URL_BASE; ?>/assets/css/theme-blog.css">
  <link rel="stylesheet" href="<?php echo URL_BASE; ?>/assets/css/theme-shop.css">

  <!-- Current Page CSS -->
  <link rel="stylesheet" href="<?php echo URL_BASE; ?>/assets/vendor/rs-plugin/css/settings.css">
  <link rel="stylesheet" href="<?php echo URL_BASE; ?>/assets/vendor/rs-plugin/css/layers.css">
  <link rel="stylesheet" href="<?php echo URL_BASE; ?>/assets/vendor/rs-plugin/css/navigation.css">

  <!-- Demo CSS -->

  <!-- Skin CSS -->
  <link rel="stylesheet" href="<?php echo URL_BASE; ?>/assets/css/skins/default.css">

  <!-- Theme Custom CSS -->
  <link rel="stylesheet" href="<?php echo URL_BASE; ?>/assets/css/custom.css">

  <!-- Head Libs -->
  <script src="<?php echo URL_BASE; ?>/assets/vendor/modernizr/modernizr.min.js"></script>

</head>
	<body>

		<div class="body coming-soon">
			<header id="header" data-plugin-options="{'stickyEnabled': false}">
				<div class="header-body border border-top-0 border-right-0 border-left-0">
					<div class="header-container container py-2">
						<div class="header-row">
							<div class="header-column">
								<div class="header-row">
									<p class="mb-0"><a href="#">info@eurotecnologia.pt</a></span></p>
								</div>
							</div>
							<div class="header-column justify-content-end">
								<div class="header-row">
									<ul class="header-social-icons social-icons mr-2">
										<li class="social-icons-facebook"><a href="http://www.facebook.com/" target="_blank" title="Facebook"><i class="fab fa-facebook-f"></i></a></li>
										<li class="social-icons-twitter"><a href="http://www.twitter.com/" target="_blank" title="Twitter"><i class="fab fa-twitter"></i></a></li>
										<li class="social-icons-linkedin"><a href="http://www.linkedin.com/" target="_blank" title="Linkedin"><i class="fab fa-linkedin-in"></i></a></li>
									</ul>
									<div class="header-nav-features">
										<div class="header-nav-features-search-reveal-container">

										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="header-nav-features header-nav-features-no-border p-static">
					<div class="header-nav-feature header-nav-features-search header-nav-features-search-reveal header-nav-features-search-reveal-big-search header-nav-features-search-reveal-big-search-full">
						<div class="container">
							<div class="row h-100 d-flex">
								<div class="col h-100 d-flex">
									<form role="search" class="d-flex h-100 w-100" action="page-search-results.html" method="get">
										<div class="big-search-header input-group">
											<input class="form-control text-1" id="headerSearch" name="q" type="search" value="" placeholder="Type and hit enter...">
											<a href="#" class="header-nav-features-search-hide-icon"><i class="fas fa-times header-nav-top-icon"></i></a>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</header>

			<div role="main" class="main" style="min-height: calc(100vh - 393px);">
				<div class="container">
					<div class="row mt-5">
						<div class="col text-center">
							<div class="logo">
								<a href="index.html">
									<img width="300" src="<?php echo URL_BASE; ?>/assets/img/logo-dark.png" alt="Eurotecnologia">
								</a>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col">
							<hr class="solid my-5">
						</div>
					</div>
					<div class="row">
						<div class="col text-center">
							<div class="overflow-hidden mb-2">
								<h2 class="font-weight-normal text-7 mb-0 appear-animation" data-appear-animation="maskUp"><strong class="font-weight-extra-bold">Modo de Manutenção</strong></h2>
							</div>
							<div class="overflow-hidden mb-1">
								<p class="lead mb-0 appear-animation" data-appear-animation="maskUp" data-appear-animation-delay="200">O site está em manutenção programada.<br>Por favor, volte mais tarde.</p>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col">
							<hr class="solid my-5">
						</div>
					</div>
					<div class="row">
						<div class="col">
							<div class="row">
								<div class="col-lg-4 appear-animation" data-appear-animation="fadeInLeftShorter" data-appear-animation-delay="600">
									<div class="feature-box">
										<div class="feature-box-icon">
											<i class="far fa-life-ring"></i>
										</div>
										<div class="feature-box-info">
											<h4 class="text-4 text-uppercase mb-1 font-weight-bold">O QUE É MANUTENÇÃO?</h4>
											<p class="mb-4">O significado técnico de manutenção envolve verificações funcionais, manutenção, reparo ou substituição de dispositivos, equipamentos, maquinaria e / ou infraestrutura.</p>
										</div>
									</div>
								</div>
								<div class="col-lg-4 appear-animation" data-appear-animation="fadeIn" data-appear-animation-delay="400">
									<div class="feature-box">
										<div class="feature-box-icon">
											<i class="far fa-clock"></i>
										</div>
										<div class="feature-box-info">
											<h4 class="text-4 text-uppercase mb-1 font-weight-bold">VOLTE MAIS TARDE</h4>
											<p class="mb-4">Não deve demorar muito tempo para o site voltar a funcionar. Enquanto isso, verifique os nossos contactos, para assim poder entrar em contacto com a nossa empresa.</p>
										</div>
									</div>
								</div>
								<div class="col-lg-4 appear-animation" data-appear-animation="fadeInRightShorter" data-appear-animation-delay="600">
									<div class="feature-box">
										<div class="feature-box-icon">
											<i class="far fa-envelope"></i>
										</div>
										<div class="feature-box-info">
											<h4 class="text-4 text-uppercase mb-1 font-weight-bold">CONTACTOS</h4>
											<p class="mb-4">No momento o melhor endereço de contacto é através do correio eletrónico info@eurotecnologia.pt. Responderemos com a maior brevidade possível às suas questões. Obrigado.</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<footer id="footer">
				<div class="container">
					<div class="row py-5">
						<div class="col-md-4 d-flex justify-content-center justify-content-md-start mb-4 mb-lg-0">
							<a href="index.html" class="logo pr-0 pr-lg-3 pl-3 pl-md-0">
								<img alt="Eurotecnologia" src="<?php echo URL_BASE; ?>/assets/img/logos/header-logo.svg" height="33">
							</a>
						</div>
						<div class="col-md-8 d-flex justify-content-center justify-content-md-end mb-4 mb-lg-0">
							<div class="row">
                <div class="col-md-6 mb-3 mb-md-0">
									<div class="ml-3 text-center text-md-right">
										<h5 class="text-3 mb-0 text-color-light">Portugal</h5>
										<p class="text-4 mb-0"><a href="mailto:info@eurotecnologia.pt" class="opacity-7 pl-1">info@eurotecnologia.pt</a></p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="footer-copyright footer-copyright-style-2">
					<div class="container py-2">
						<div class="row py-4">
							<div class="col-md-4 d-flex align-items-center justify-content-center justify-content-md-start mb-2 mb-lg-0">
								<p>© Copyright 2020. All Rights Reserved.</p>
							</div>
							<div class="col-md-8 d-flex align-items-center justify-content-center justify-content-md-end mb-4 mb-lg-0">
								<p><i class="far fa-envelope text-color-primary top-1 p-relative"></i><a href="mailto:info@eurotecnologia.pt" class="opacity-7 pl-1">info@eurotecnologia.pt</a></p>
								<ul class="footer-social-icons social-icons social-icons-clean social-icons-icon-light ml-3">
									<li class="social-icons-facebook"><a href="http://www.facebook.com/" target="_blank" title="Facebook"><i class="fab fa-facebook-f"></i></a></li>
									<li class="social-icons-twitter"><a href="http://www.twitter.com/" target="_blank" title="Twitter"><i class="fab fa-twitter"></i></a></li>
									<li class="social-icons-linkedin"><a href="http://www.linkedin.com/" target="_blank" title="Linkedin"><i class="fab fa-linkedin-in"></i></a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</footer>
		</div>

    <!-- Vendor -->
    <script src="<?php echo URL_BASE; ?>/assets/vendor/jquery/jquery.min.js"></script>
    <script src="<?php echo URL_BASE; ?>/assets/vendor/jquery.appear/jquery.appear.min.js"></script>
    <script src="<?php echo URL_BASE; ?>/assets/vendor/jquery.easing/jquery.easing.min.js"></script>
    <script src="<?php echo URL_BASE; ?>/assets/vendor/jquery.cookie/jquery.cookie.min.js"></script>
    <script src="<?php echo URL_BASE; ?>/assets/vendor/popper/umd/popper.min.js"></script>
    <script src="<?php echo URL_BASE; ?>/assets/vendor/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?php echo URL_BASE; ?>/assets/vendor/common/common.min.js"></script>
    <script src="<?php echo URL_BASE; ?>/assets/vendor/jquery.validation/jquery.validate.min.js"></script>
    <script src="<?php echo URL_BASE; ?>/assets/vendor/jquery.easy-pie-chart/jquery.easypiechart.min.js"></script>
    <script src="<?php echo URL_BASE; ?>/assets/vendor/jquery.gmap/jquery.gmap.min.js"></script>
    <script src="<?php echo URL_BASE; ?>/assets/vendor/jquery.lazyload/jquery.lazyload.min.js"></script>
    <script src="<?php echo URL_BASE; ?>/assets/vendor/isotope/jquery.isotope.min.js"></script>
    <script src="<?php echo URL_BASE; ?>/assets/vendor/owl.carousel/owl.carousel.min.js"></script>
    <script src="<?php echo URL_BASE; ?>/assets/vendor/magnific-popup/jquery.magnific-popup.min.js"></script>
    <script src="<?php echo URL_BASE; ?>/assets/vendor/vide/jquery.vide.min.js"></script>
    <script src="<?php echo URL_BASE; ?>/assets/vendor/vivus/vivus.min.js"></script>

    <!-- Theme Base, Components and Settings -->
    <script src="<?php echo URL_BASE; ?>/assets/js/theme.js"></script>

    <!-- Current Page Vendor and Views -->
    <script src="<?php echo URL_BASE; ?>/assets/vendor/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
    <script src="<?php echo URL_BASE; ?>/assets/vendor/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>

    <!-- Theme Custom -->
    <script src="<?php echo URL_BASE; ?>/js/custom.js"></script>

    <!-- Theme Initialization Files -->
    <script src="<?php echo URL_BASE; ?>/assets/js/theme.init.js"></script>



    </body>
    </html>
