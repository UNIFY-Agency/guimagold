jQuery(document).ready(function() {
  //idioma
  jQuery('.lang').click(function() {
    // console.log(jQuery(this).attr('lang'));
    var url = URL_BASE + '/language/change/' + jQuery(this).attr('lang');
    jQuery.post(url, function() {
      location.reload();
    });
  });

  jQuery('.cookie-ok').click(function() {
    //console.log(jQuery(this).attr('class'));
    var url = URL_BASE + '/cookies/allow/';
    jQuery.post(url, function() {
      jQuery('.notice-bottom-bar').hide();
    });
  });

  jQuery('.header-search-mobile').click(function() {
    console.log('clique');
    jQuery('.search-line').toggle("slow");
  });
});

$("#recruitmentForm").on('submit',(function(e) {
    e.preventDefault();

    var btn = $('.recruitmentForm-submit');
    btn.prop('disabled', true);
    $(btn).html($(btn).attr("data-loading-text"));
    setTimeout(function(){
        btn.prop('disabled', false);
        $(btn).html('<i class="far fa-envelope mr-2"></i>Enviar');
    }, 3*2000);

    var dados = $("#recruitmentForm").serializeArray();
    dados.push({name: "user_session_id", value: USER_SID});

    console.log(dados);

    $.post( URL_BASE + '/Site/recruitment', {dados}, function( result ) {
        console.log(result);
        var results = jQuery.parseJSON(result);
        if(results[0] == 0) {
          $("#notifications").removeClass().addClass("alert alert-danger").html(results[1]).fadeIn().delay(4000).fadeOut();
          document.getElementById("recruitmentForm").reset();
        } else {
          $("#notifications").removeClass().addClass("alert alert-success").html(results[1]).fadeIn().delay(2000).fadeOut();
          document.getElementById("recruitmentForm").reset();
        }
    });

}));

$("#contactForm").on('submit',(function(e) {
    e.preventDefault();

    var btn = $('#contactForm-submit');
    btn.prop('disabled', true);
    $(btn).html($(btn).attr("data-loading-text"));
    setTimeout(function(){
        btn.prop('disabled', false);
        $(btn).html('<i class="far fa-envelope mr-2"></i>Enviar');
    }, 3*2000);

    var dados = $("#contactForm").serializeArray();
    dados.push({name: "user_session_id", value: USER_SID});

    console.log(dados);

    $.post( URL_BASE + '/Site/send', {dados}, function( result ) {
        console.log(result);
        var results = jQuery.parseJSON(result);
        if(results[0] == 0) {
          $("#notifications").removeClass().addClass("alert alert-danger").html(results[1]).fadeIn().delay(4000).fadeOut();
          document.getElementById("contactForm").reset();
        } else {
          $("#notifications").removeClass().addClass("alert alert-success").html(results[1]).fadeIn().delay(2000).fadeOut();
          document.getElementById("contactForm").reset();
        }
    });

}));
